#!/usr/bin/perl

use strict;
use warnings;

use Mojolicious::Lite;
use BackPAN::Index;
use YAML;

my $options = {
	index     => $ENV{DARKPAN_INDEX} || 'backpan-index.txt',
	cache_dir => $ENV{DARKPAN_CACHEDIR} || 'cache'
};

sub _format_line {
	my(@row) = @_;

	# from cpanmetadb-perl https://github.com/miyagawa/cpanmetadb-perl/blob/master/app.pl
	# from PAUSE::mldistwatch::rewrite02
	my $one = 30;
	my $two = 8;
	if (length $row[0] > $one) {
		$one += 8 - length $row[1];
		$two = length $row[1];
	}

	sprintf "%-${one}s %${two}s  %s\n", @row;
}

get '/v1.0/history/:package' => sub {
	my $c = shift;
	my $package = $c->param('package');

	$package =~ s/::/-/g;

	my $backpan = BackPAN::Index->new(
	        backpan_index_url          => $options->{index},
	        cache_dir                  => $options->{cache_dir},
	        update                     => 0,
	        releases_only_from_authors => 0
	);

	my $dist = $backpan->dist($package);
	if(!defined $dist) {
		$c->redirect_to(sprintf 'http://cpanmetadb.plackperl.org/v1.0/history/%s', $package);
		return;
	}

	my @releases = $dist->releases->search(undef, { 
		order_by => "version" 
	});

	my $data = '';
	for my $release (@releases) {
		my($version) = $release->version =~ m/([^-]+)-/;
		my $dist = $release->dist;
		$dist =~ s/-/::/g;
		$data .= _format_line( $dist, $version, $release->path );
	}

	$c->res->headers->content_type('text/plain');
	$c->res->headers->add('Cache-Control' => 'max-age=1800');
	$c->render(data => $data);
};

get '/v1.0/package/:package' => sub {
        my $c = shift;
        my $package = $c->param('package');

	$package =~ s/::/-/g;

        my $backpan = BackPAN::Index->new(
                backpan_index_url          => $options->{index},
                cache_dir                  => $options->{cache_dir},
                update                     => 0,
                releases_only_from_authors => 0
        );

        my $dist = $backpan->dist($package);
	if(!defined $dist) {
                $c->redirect_to(sprintf 'http://cpanmetadb.plackperl.org/v1.0/package/%s', $package);
                return;
        }

	my $latest = $dist->latest_release;
	my ($version) = $latest->version =~ m/([^-]+)-/;
	my $data = {
		distfile => $latest->path.'',
	        version  => $version,
        	provides => undef #no data
	};

        $c->res->headers->content_type('text/plain');
        $c->res->headers->add('Cache-Control' => 'max-age=1800');
        $c->render(data => YAML::Dump($data));
};

app->start;
