# README #
This Dockerfile build docker container with cpan-mirror-tiny-server https://metacpan.org/pod/CPAN::Mirror::Tiny::Server

## Preparing ##
Clone this repo
```
#!bash
git clone git@bitbucket.org:zaytsew_ilya/cpan-mirror-server.git
cd cpan-mirror-server
```

Create persistent data-volume
```
#!bash

docker create -v /darkpan -v /darkbackpan --name cpan-mirror-storage perl:latest
```

Create a new isolated network
```
#!bash

docker network create -d bridge cpan-mirror-network
```

Generate rsa key
```
#!bash
ssh-keygen -t rsa
```
Add id_rsa.pub to your bitbucket account

## Mirror server ##
```
#!bash

cd server
```
Copy here private key as "id_rsa"

Build cpan-mirror-server
```
#!bash
docker build -t cpan-mirror-server .
```

Start cpan mirror server
```
#!bash
docker run -d --network cpan-mirror-network --volumes-from cpan-mirror-storage --restart unless-stopped --name cpan-mirror cpan-mirror-server
```

## Webhook ##
Change dir to webhook 
```
#!bash

cd ../webhook
```
Build
```
#!bash

docker build -t cpan-mirror-webhook .
```

Run container in the our new network
```
#!bash

docker run -d --network cpan-mirror-network --restart unless-stopped --name cpan-mirror-webhook cpan-mirror-webhook
```

## Darkbackpan ##
If we need to provide older version of packages we need a private backpan of our mirror
```
#!bash
cd ../backpan
docker build -t cpan-mirror-scheduler .
```
Scheduler have a daemon, thar fires rsync every day between our mirror and its copy. After rsync it builds index and saves it to sqlite database. So darkbackpandb can access index meta information.

Run
```
#!bash

docker run -d --volumes-from cpan-mirror-storage --restart unless-stopped --network cpan-mirror-network --name cpan-mirror-scheduler cpan-mirror-scheduler
```

Update metacpandb by hand
```
#!bash
docker run -it --rm \
   --volumes-from cpan-mirror-storage \
   --network cpan-mirror-network \ 
   cpan-mirror-scheduler carton exec perl script/index.pl --index-filename /darkbackpan/backpan-index.txt --cache-dir /darkbackpan/cache --debug --rsync --rsync-from /darkpan --rsync-to /darkbackpan --update
```

## Darkbackpandb ##
API service like http://cpanmetadb.plackperl.org/
It provides simple APIs:

/v1.0/package/Package::Name 
returns YAML with distfile, provides and version fields of latest release

/v1.0/history/Package::Name
returns the list of all package releases

```
#!bash
cd ../backpan-api
docker build -t cpan-mirror-darkpandb .
```

Run
```
#!bash

docker run -d --volumes-from cpan-mirror-storage --restart unless-stopped --network cpan-mirror-network --name cpan-mirror-darkpandb cpan-mirror-darkpandb
```

## Proxy ##
Change directory to proxy
```
#!bash

cd ../proxy
```
Create basic auth file

```
#!bash

htpasswd -c cpan-mirror-webhook.password bitbucket
```
Build it
```
#!bash

docker build -t cpan-mirror-proxy .
```

And run it
```
#!bash
docker run -d -p 8080:80 -p 8081:81 --restart unless-stopped --network cpan-mirror-network --name cpan-mirror-proxy cpan-mirror-proxy
```