#!/usr/bin/env perl

use warnings;
use strict;

use AnyEvent;
use Getopt::Long;
use DateTime::Event::Cron;

my $schedule;
my $cmd;

GetOptions (
	"schedule=s" => \$schedule,
	"cmd=s"      => \$cmd
);

die 'empty cmd' unless $cmd;

my $cb = sub {
	my $c = DateTime::Event::Cron->new($schedule);
	if($c->match) {
		system($cmd);
	}
};

my $quit = AnyEvent->condvar;

my $w = AnyEvent->timer(
	after => 0,
	interval => 60,
	cb => $cb
);

$quit->recv;
