#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Long;
use BackPAN::Index;
use URI::file;

my $options = {
	only_authors   => 0,
	update         => 0,
	cache_dir      => 'cache',
	rsync          => 0,
	rsync_from     => '',
	rsync_to       => '',
	index_filename => 'backpan-index.txt',
	debug          => 0
};

GetOptions(
	'only-authors'     => \$options->{only_authors},
	'cache-dir=s'      => \$options->{cache_dir},
	'update'           => \$options->{update},
	'rsync'            => \$options->{rsync},
	'rsync-from=s'     => \$options->{rsync_from},
	'rsync-to=s'       => \$options->{rsync_to},
	'index-filename=s' => \$options->{index_filename},
	'debug'            => sub { $options->{debug} = 1 }
);

sub _debug {
	my(@args) = @_;

	if($options->{debug}) {
		print join ' ',@_, "\n";
	}
}

sub _run {
	my(@args) = @_;
	
	_debug('RUN', @args);
	system(@args);
}

if($options->{rsync} && $options->{rsync_from} && $options->{rsync_to}) {
	_run("rsync", "-av", "$options->{rsync_from}/authors", $options->{rsync_to});

	
	_run(
		'carton', 'exec', 'local/bin/create-backpan-index', 
		'-basedir', $options->{rsync_to}, 
		'-releases-only', '0', 
		'-output', $options->{index_filename}, 
		'-order', 'dist'
	);

	_run('gzip', '-f', $options->{index_filename});
}

if($options->{update}) {
	my $uri = URI::file->new($options->{index_filename}.'.gz')->abs(URI::file->cwd);

	_debug('Update darkmetadb from', $uri);
	BackPAN::Index->new(
		backpan_index_url          => $uri,
		cache_dir                  => $options->{cache_dir},
		update                     => $options->{update},
		releases_only_from_authors => $options->{only_authors},
		cache_ttl                  => 0
	);
}
