#!/usr/bin/env perl

use warnings;
use strict;

use Plack::Runner;
use Plack::Request;
use Plack::Builder;
use JSON::XS qw(decode_json);
use HTTP::Tiny;

my $runner = Plack::Runner->new;
$runner->parse_options(@ARGV);

sub hook {
        return sub {
                my $env = shift;
                my $req = Plack::Request->new($env);

                eval {
                        my $payload = decode_json $req->content;

                        my $author = $payload->{repository}{owner}{username};
                        my $module = sprintf 'git@bitbucket.org:%s.git', $payload->{repository}{full_name};

                        if($author && $module) {
                                my $r = HTTP::Tiny->new->post_form('http://cpan-mirror:5000/upload', {
                                        author => $author,
                                        module => $module
                                });
                                if($r->{success}) {
                                        return [200, [], ['OK']];
                                } else {
                                        return [500, [], [$r->{content}]];
                                }
                        }
                };
                if (my $err = $@) {
                        warn $err . '';
                        return [500, [], [$err.'']];
                }

                return [200, [], ['OK']];
        };
}

my $app = builder {
        mount "/hook" => hook();
};

$runner->run($app);
